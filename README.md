<div align='center'>
    <h1 align='center'>
    Eai, sou o William Bierhals!
    <br/>
    Prazer ter você por aqui!
    </h1>  
</div>
<div align='center'>
    <p align='center'>
        QA and Frontend developer.
    </p>     
</div>


### ✨ Sobre mim:

<p>
  <em>
    Tenho 21 anos e estou indo atrás do meu sonho de me tornar desenvolvedor.
  </em>
</p>

- 📚 Aprendendo sobre QA, automação de teste e desenvolvimento web.
- 💻 Cursando Análise e Desenvolvimento de Sistemas na UCPel.
- 📫 Meu email: willambierhals@gmail.com

### 📊 Metas 2023:

- 🧩 Construir projetos pessoais.

- 📂 Criar meu portfólio.

- 📈 Conseguir contratação na área.

### ⚡ Tecnologias:

Estas são algumas das tecnologias e ferramentas que trabalho:

[![Minhas Habilidades](https://skillicons.dev/icons?i=html,css,javascript,react,firebase,bootstrap,styledcomponents,ts,java,php,mysql)](https://skillicons.dev)

💬 Quer me conhecer?

<div>
  <a href="https://www.linkedin.com/in/william-bierhals-971b84222/" target="_blank"><img src="https://img.shields.io/badge/-LinkedIn-%230077B5?style=for-the-badge&logo=linkedin&logoColor=white" target="_blank"></a>
  <a href = "mailto:willambierhals@gmail.com"><img src="https://img.shields.io/badge/-Gmail-%23333?style=for-the-badge&logo=gmail&logoColor=white" target="_blank"></a>
</div>
